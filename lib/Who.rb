module Who
    def self.current_user
        header = request.headers["Authorization"]
        header = header.split(' ').last if header
    
        return nil unless header.present?
        @decoded = JsonWebToken.decode(header)
    
        return nil unless @decoded.present?
        user = User.find_by(id: @decoded[0]["user_id"])
    end
end