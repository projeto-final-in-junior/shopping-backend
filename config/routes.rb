Rails.application.routes.draw do   
  resources :texts
  resources :histories
  post '/register',to: 'registration#register'
  post '/login', to: 'session#login'

  get '/users', to: 'users#index' #admin
  delete '/users/:id', to: 'users#destroy' #admin
  
  get '/profile', to: 'users#profile' #self
  put '/profile/edit', to: 'users#edit' #self

  get '/admin', to: 'dashboard#admin' #all
  delete '/admin/:id', to: 'dashboard#switch_kind' #admin
  get '/admin/graph', to: 'dashboard#graph' #admin

  
  get  '/products',      to: 'product_search#all' #all
  get  '/products/:id',   to: 'product_search#show' #all

  put  '/products/:id',   to: 'products#edit' #admin
  post '/products/add', to: 'products#create' #admin
  delete '/product/remove/:id', to: 'products#destroy' #admin

  post '/images' , to: 'images#create'
  delete '/images/:id', to:'images#destroy'


  get '/on_stock', to: 'product_search#on_stock' #all
  get '/top_3_products', to: 'product_search#most_liked'#all
  post '/product/search/price/:min/:max', to:'product_search#price_range' #all

  get '/category', to: 'product_search#index_category' #all
  post '/category/add',to: 'categories#create' #admin
  put '/category/edit/:id',to: 'categories#edit' #admin
  delete '/category/remove/:id', to: 'categories#delete' #admin

  
  get  '/kart',        to: 'shopping_karts#index' #all  
  post '/kart/add',    to: 'shopping_karts#create' #all
  put '/kart/edit',   to: 'shopping_karts#kart_edit' #all
  post '/kart/remove', to: 'shopping_karts#remove' #all
  delete '/kart/clear', to: 'shopping_karts#clear' #all
  delete '/kart/confirm', to: 'shopping_karts#confirm' #all
  get '/kart/total', to: 'shopping_karts#total_cost' #all

 
  
  post '/product/like', to: 'likes#create' #all
  post '/product/like/remove', to: 'likes#destroy' #all
  
  post '/comment/like', to: 'like_comments#create' #all
  post '/comment/like/remove', to: 'like_comments#destroy' #all
  
  post '/comment',  to: 'comments#create' #all
  put '/comment/:id', to: 'comments#update' #self
  delete '/comment/:id', to:'comments#destroy' #self
  
  post '/product/like/log', to: 'like_log#product' #all
  post '/comments/like/log', to: 'like_log#comments' #all

end
