require 'test_helper'

class ShoppingKartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shopping_kart = shopping_karts(:one)
  end

  test "should get index" do
    get shopping_karts_url, as: :json
    assert_response :success
  end

  test "should create shopping_kart" do
    assert_difference('ShoppingKart.count') do
      post shopping_karts_url, params: { shopping_kart: { product_id: @shopping_kart.product_id, quantity_to_be_bought: @shopping_kart.quantity_to_be_bought, user_id: @shopping_kart.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show shopping_kart" do
    get shopping_kart_url(@shopping_kart), as: :json
    assert_response :success
  end

  test "should update shopping_kart" do
    patch shopping_kart_url(@shopping_kart), params: { shopping_kart: { product_id: @shopping_kart.product_id, quantity_to_be_bought: @shopping_kart.quantity_to_be_bought, user_id: @shopping_kart.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy shopping_kart" do
    assert_difference('ShoppingKart.count', -1) do
      delete shopping_kart_url(@shopping_kart), as: :json
    end

    assert_response 204
  end
end
