require 'test_helper'

class HasCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @has_category = has_categories(:one)
  end

  test "should get index" do
    get has_categories_url, as: :json
    assert_response :success
  end

  test "should create has_category" do
    assert_difference('HasCategory.count') do
      post has_categories_url, params: { has_category: { category_id: @has_category.category_id, user_id: @has_category.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show has_category" do
    get has_category_url(@has_category), as: :json
    assert_response :success
  end

  test "should update has_category" do
    patch has_category_url(@has_category), params: { has_category: { category_id: @has_category.category_id, user_id: @has_category.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy has_category" do
    assert_difference('HasCategory.count', -1) do
      delete has_category_url(@has_category), as: :json
    end

    assert_response 204
  end
end
