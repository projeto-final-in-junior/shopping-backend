require 'test_helper'

class AdressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @adress = adresses(:one)
  end

  test "should get index" do
    get adresses_url, as: :json
    assert_response :success
  end

  test "should create adress" do
    assert_difference('Adress.count') do
      post adresses_url, params: { adress: { User: @adress.User, address_name: @adress.address_name, cellphone_number: @adress.cellphone_number, cep: @adress.cep, city: @adress.city, district: @adress.district, extra: @adress.extra, name: @adress.name, number: @adress.number, phone_number: @adress.phone_number, state: @adress.state, street: @adress.street } }, as: :json
    end

    assert_response 201
  end

  test "should show adress" do
    get adress_url(@adress), as: :json
    assert_response :success
  end

  test "should update adress" do
    patch adress_url(@adress), params: { adress: { User: @adress.User, address_name: @adress.address_name, cellphone_number: @adress.cellphone_number, cep: @adress.cep, city: @adress.city, district: @adress.district, extra: @adress.extra, name: @adress.name, number: @adress.number, phone_number: @adress.phone_number, state: @adress.state, street: @adress.street } }, as: :json
    assert_response 200
  end

  test "should destroy adress" do
    assert_difference('Adress.count', -1) do
      delete adress_url(@adress), as: :json
    end

    assert_response 204
  end
end
