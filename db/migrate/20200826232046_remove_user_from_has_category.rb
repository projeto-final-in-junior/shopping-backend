class RemoveUserFromHasCategory < ActiveRecord::Migration[5.2]
  def change
    remove_reference :has_categories, :user, foreign_key: true
  end
end
