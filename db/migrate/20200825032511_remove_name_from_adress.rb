class RemoveNameFromAdress < ActiveRecord::Migration[5.2]
  def change
    remove_column :adresses, :name, :string
  end
end
