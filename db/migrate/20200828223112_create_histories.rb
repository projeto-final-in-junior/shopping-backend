class CreateHistories < ActiveRecord::Migration[5.2]
  
  def change
    create_table :histories do |t|
      t.string :product_name
      t.integer :quantity
      t.float :price
      t.boolean :descontinued

      t.timestamps
    end
  end
end
