class AddCellPhoneToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :cellphone_number, :string
  end
end
