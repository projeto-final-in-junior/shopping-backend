class CreateShoppingKarts < ActiveRecord::Migration[5.2]
  def change
    create_table :shopping_karts do |t|
      t.references :user, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity_to_be_bought, default: 1

      t.timestamps
    end
  end
end
