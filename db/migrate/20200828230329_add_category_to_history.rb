class AddCategoryToHistory < ActiveRecord::Migration[5.2]
  def change
    add_column :histories, :category_name, :string
  end
end
