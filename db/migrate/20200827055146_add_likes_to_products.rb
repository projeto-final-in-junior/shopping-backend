class AddLikesToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :like_amount, :integer, default: 0
  end
end
