class DropHasCategory < ActiveRecord::Migration[5.2]
  def up
    drop_table :has_categories
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
