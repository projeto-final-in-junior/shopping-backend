class RemoveNumberIntFromAdress < ActiveRecord::Migration[5.2]
  def change
    remove_column :adresses, :number, :integer
  end
end
