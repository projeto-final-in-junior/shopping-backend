class AddMenuNameToAdress < ActiveRecord::Migration[5.2]
  def change
    add_column :adresses, :menu_name, :string
  end
end
