class RemovePhoneFromAdress < ActiveRecord::Migration[5.2]
  def change
    remove_column :adresses, :phone_number, :string
  end
end
