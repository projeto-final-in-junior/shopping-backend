class RemoveAdressNameFromAdress < ActiveRecord::Migration[5.2]
  def change
    remove_column :adresses, :address_name, :string
  end
end
