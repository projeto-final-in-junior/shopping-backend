class CreateTexts < ActiveRecord::Migration[5.2]
  def change
    create_table :texts do |t|
      t.string :content
      t.string :section

      t.timestamps
    end
  end
end
