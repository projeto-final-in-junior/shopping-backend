class RemoveCellPhoneFromAdress < ActiveRecord::Migration[5.2]
  def change
    remove_column :adresses, :cellphone_number, :string
  end
end
