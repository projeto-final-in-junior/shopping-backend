class AddNumberToAdress < ActiveRecord::Migration[5.2]
  def change
    add_column :adresses, :number, :string
  end
end
