class CreateAdresses < ActiveRecord::Migration[5.2]
  def change
    create_table :adresses do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :cep
      t.string :district
      t.string :city
      t.string :state
      t.string :street
      t.string :extra
      t.integer :number
      t.string :phone_number
      t.string :cellphone_number
      t.string :address_name

      t.timestamps
    end
  end
end
