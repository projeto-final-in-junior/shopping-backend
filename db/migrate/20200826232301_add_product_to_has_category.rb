class AddProductToHasCategory < ActiveRecord::Migration[5.2]
  def change
    add_reference :has_categories, :product, foreign_key: true
  end
end
