# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: "Admin",
second_name: "istrador",
email: "admin@admin.com",
password: "123456",
password_confirmation: "123456", kind: 0)

User.create(name: "Admin",
    second_name: "istrador",
    email: "admin@deb.com",
    password: "123456",
    password_confirmation: "123456", kind: 0)

User.create(name: "Carrilho",
    second_name: "Rafael",
    email: "carrilho@in.junior.com",
    password: "123456",
    password_confirmation: "123456", kind: 0)

User.create(name: "Lucas",
    second_name: "Tavares",
    email: "tavares@in.junior.com",
    password: "123456",
    password_confirmation: "123456", kind: 0)

User.create(name: "Nakao",
    second_name: "Arthur",
    email: "nakao@in.junior.com",
    password: "123456",
    password_confirmation: "123456", kind: 0)

User.create(name: "User",
    second_name: "Padrão",
    email: "padrão@in.junior.com",
    password: "123456",
    password_confirmation: "123456", kind: 0)
