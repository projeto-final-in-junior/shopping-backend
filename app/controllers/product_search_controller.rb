class ProductSearchController < ApplicationController
    before_action :set_product, only: [:show]

    def most_liked
        all = Product.all
        result = []
        cycle = 0
        highest = Product.where(like_amount: all.maximum("like_amount")) 
        if highest.length > 3
            while cycle < 3
                result.push(highest[cycle])
                cycle += 1
            end
            cycle = 0
        else
            for x in highest
                result.push(x)
            end
            likes = all.maximum("like_amount") - 1
            while result.length < 3 && likes > -1
                y = Product.where(like_amount: Product.where(like_amount: 0..likes).maximum("like_amount"))                
                while cycle < 3 && cycle < y.length && result.length < 3
                    result.push(y[cycle])
                    cycle += 1
                end
                likes -= 1
            end
        end
        render json: result
    end
    

    def price_range
        min = params[:min].to_f
        max = params[:max].to_f
        if min < 0 || max < 0
            render json: {message: "Max & Min devem ser numeros maiores que 0"}, status: 400        
        elsif min > max
            render json: {message: "Max deve ser maior que min"}, status: 400
        else
            @result = Product.where(price: min..max)
            render json: @result   
        end  
    end

    def on_stock
        on_stock = Product.where(quantity: 1..Product.maximum("quantity"))
        render json: on_stock 
    end
    
    def all
        @products = Product.all

        render json: @products
    end

    def show
        render json: @product
    end

    def index_category
        @categories = Category.all
    
        render json: @categories
        
    end


    private
        def set_product
            @product = Product.find(params[:id])
        end
end
