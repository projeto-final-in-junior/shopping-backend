class LikeCommentsController < ApplicationController
  before_action :set_like_comment, only: [:show, :update, :destroy]
  before_action :must_be_signed_in

  # POST /like_comments
  def create
    x = like_comment_params
    params = {
      user_id: current_user.id,
      comment_id: x["comment_id"]
    }
    @like_comment = LikeComment.new(params)
    if @like_comment.save
      render json: @like_comment, status: :created
    else
      render json: @like_comment.errors, status: :unprocessable_entity
    end
  
  end

  # PATCH/PUT /like_comments/1
  def update
    if @like_comment.update(like_comment_params)
      render json: @like_comment
    else
      render json: @like_comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE
  def destroy
    @like_comment = LikeComment.find_by(comment_id: like_comment_params["comment_id"],user_id: current_user.id)
    unless @like_comment.nil?
      @like_comment.destroy 
    else
      render status: 402
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like_comment
      @like_comment = LikeComment.all
    end

    # Only allow a trusted parameter "white list" through.
    def like_comment_params
      params.require(:like).permit(:comment_id)
    end
end
