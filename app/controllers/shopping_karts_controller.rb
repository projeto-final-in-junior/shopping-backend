class ShoppingKartsController < ApplicationController
  before_action :set_shopping_kart, only: [:show, :update, :destroy]
  before_action :must_be_signed_in

  def stock
    @shopping_kart = ShoppingKart.where(user_id: current_user.id)
    @possible = true

    @shopping_kart.each do |kart|
      if kart["quantity_to_be_bought"] > Product.find_by!(id: kart["product_id"]).quantity
        @possible = false
        ShoppingKart.find(kart["id"]).save
      end
    end

    return @possible
  end
 
  def confirm
    @shopping_kart = ShoppingKart.where(user_id: current_user.id)
    if stock == true && @shopping_kart.present?
      @shopping_kart.each do |kart|
        @product = Product.find_by!(id: kart["product_id"])
        @product.update_quantity(kart["quantity_to_be_bought"]) 
        @category = Category.find(@product.category_id).name
        History.create(product_name:@product.name, price:@product.price, category_name:@category, descontinued: false, quantity:kart["quantity_to_be_bought"])
      end
        clear
        PaymentMailer.with(user: current_user).confirm.deliver_now
        render json: {message: "Compra confirmada"}
    else
      render json: {message: "Alguns de nossos produtos podem ter sido vendidos ou removidos durante suas compras e não há quantidade de produtos suficiente para a compra ou não havia nada no carrinho"}, status: 422
    end
  end
  
  def index
    @shopping_kart = ShoppingKart.where(user_id: current_user.id)
    
    render json: @shopping_kart
  end

 
  def create
    x = shopping_kart_params
    params = {
      user_id: current_user.id,
      product_id: x["product_id"],
      quantity_to_be_bought: x["quantity_to_be_bought"]
    }
    @shopping_kart = ShoppingKart.new(params)

    if @shopping_kart.save
      render json: @shopping_kart, status: :created
    else
      render json: @shopping_kart.errors, status: :unprocessable_entity
    end
  end

 
  def kart_edit
    x = shopping_kart_params
    @product_on_kart = ShoppingKart.find_by(product_id: x["product_id"],  user_id: current_user.id)
    if @product_on_kart.present?
      @product_on_kart.update_attribute("quantity_to_be_bought", x["quantity_to_be_bought"])

      render json: @product_on_kart, status: :created
    else
      render json: {message: "Produto não foi encontrado"}, status: 400
    end
  end

  def remove
    x = shopping_kart_params
    @product_on_kart = ShoppingKart.find_by(product_id: x["product_id"],  user_id: current_user.id)
    if @product_on_kart.present?
      @product_on_kart.destroy
    else
      render json: {message: "Produto não foi encontrado"}, status: 404
    end

  end

  def clear
    @user = current_user
    @kart = ShoppingKart.where(user_id: @user.id)
    unless @kart.nil?
      @kart.destroy_all
    else
      render json: {message: "Carrinho já vazio"}
    end
  end

  def total_cost
    @kart = ShoppingKart.where(user_id: current_user.id)
    total = 0
    for x in @kart
      total += Product.find(x.product_id).price * x.quantity_to_be_bought
    end
    render json: total
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shopping_kart
      @shopping_kart = ShoppingKart.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def shopping_kart_params
      params.require(:kart).permit(:product_id, :quantity_to_be_bought)
    end
end
