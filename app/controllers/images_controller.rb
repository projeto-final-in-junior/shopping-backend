class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource

  # GET /images
  def index
    @images = Image.all
    
    render json: @images
  end
  
  # GET /images/1
  def show
    render json: @image
  end
  
  # POST /images
  def create
    @image = Image.new(image_params)    

    if @image.save
      render json: @image, status: :created
    else
      render json: @image.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /images/1
  def update
    if @image.update(image_params)
      render json: @image
    else
      render json: @image.errors, status: :unprocessable_entity
    end
  end

  # DELETE /images/1
  def destroy
    @image.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def image_params
      params.permit(:product_id, :picture)
    end
end
