class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource
  skip_authorize_resource :only => [:edit, :profile]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end


  def profile
    @profile = User.find(current_user.id)

    render json: @profile
  end
  
  

  # PATCH/PUT /users/1
  def edit
    @user = User.find(current_user.id)
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :second_name, :email, :password, :password_confirmation, :phone_number, :cellphone_number)
    end
end
