class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource


  # POST /categories
  def create
    @category = Category.new(category_params)

    if @category.save
      render json: @category, status: :created
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def delete 
    begin
      set_category.destroy
    rescue => exception
      render json: {message: "Não pode deletar uma categoria que tem produtos vinculados a ele, ou ela não existe"}, status: 405
    end
  end

  def edit
    begin   
      set_category.update_attributes(name: category_params['name'])

      render json: set_category
    rescue => exception
      render json: {message: "Essa categoria não existe"}, status: 400
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find_by(id: params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name)
    end
end
