class LikeLogController < ApplicationController
    before_action :must_be_signed_in

    def product
        if Like.find_by(product_id: log_params["product_id"],user_id: current_user.id).present?
            render json: true
        else
            render json: false
        end
    end
    def comments
        comments = Comment.where(product_id: log_params["product_id"]) ##Comentários no produto x
        liked = [] ##Liked by user (token)
        unless comments.nil?
            for x in comments
                if LikeComment.find_by(user_id: current_user.id, comment_id: x.id).present?
                    liked.push(x.id)
                end
            end
        end
        render json: liked
    end

private 
    def log_params
        params.require(:log).permit(:product_id)
    end
end
