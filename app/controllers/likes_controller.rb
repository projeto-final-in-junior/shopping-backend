class LikesController < ApplicationController
  before_action :set_like, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  
  # POST /likes
  def create
    x = like_params
    params = {
      user_id: current_user.id,
      product_id: x["product_id"]
    }
    @like = Like.new(params)

    if @like.save
      render json: @like, status: :created
    else
      render json: @like.errors, status: :unprocessable_entity
    end
  end

  # DELETE /likes/1
  def destroy
    begin
      like = Like.find_by(product_id: like_params["product_id"], user_id: current_user.id) 
      like.destroy
    rescue => exception
      render json: {message: "Não é possivel retirar um like se o like não existe"}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like
      @like = Like.all
    end

    # Only allow a trusted parameter "white list" through.
    def like_params
      params.require(:like).permit(:user_id, :product_id)
    end
end
