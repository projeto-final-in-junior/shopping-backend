class DashboardController < ApplicationController
  before_action :the_user, only: [:switch_kind]
  before_action :must_be_signed_in
  #load_and_authorize_resource   #######Load_and_authorize levanta um erro 500????


  def admin ##all
    if current_user.admin?
        render json: true
    else
        render json: false
    end
  end

  def switch_kind
    if current_user.admin? && current_user.id != @user.id
      if @user.admin?
        @user.update_attributes!(kind: "user")
      else
        @user.update_attributes!(kind: "admin") 
      end
      @user.save
      render json: @user
    else
      render json: {message: "Não é um admin"}, status: 403
    end
  end

  def graph
    if current_user.admin?
      result = []
      
      categorias = [] ##categorias existentes
      produtos_por_categoria = []

      
      money_por_categoria = []
      vendas_por_categoria = []
      for category in Category.all
        categorias.push(category.name)
      end
      for name in categorias
        produtos_por_categoria.push(History.where(category_name: name, descontinued: false))
      end
      descontinuados = History.where(descontinued: true)
      produtos_por_categoria.push(descontinuados)
      categorias.push("Descontinuados")
      
      for categoria in produtos_por_categoria
        preço_total = 0
        quantidade_total = 0
        for product in categoria
          preço_total += product.price * product.quantity
          quantidade_total += product.quantity
        end
        money_por_categoria.push(preço_total)
        vendas_por_categoria.push(quantidade_total)
      end
      for x in 0..categorias.length - 1
        
        a = {
          Categoria: categorias[x],
          numero_de_vendas: vendas_por_categoria[x],
          valor_total_vendido: money_por_categoria[x]
        }
        result.push(a)
      end
      
      render json: result
    else 
      render json: {message: "Não autorizado"}, status: 403
    end
  end

  private
    def the_user
      @user = User.find(params[:id])
    end 
end
