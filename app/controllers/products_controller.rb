class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource

  
  def create
    @product = Product.new(product_params)
    

    if @product.save
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @product.destroy
  end

  def edit
    begin
      set_product.update(product_params)
      render json: set_product
    rescue => exception
      render json: {message: "Nao existe o produto"}
    end
  end
  
  private
    def set_product
      @product = Product.find(params[:id])
    end
    
    def product_params
      params.require(:product).permit(:name, :price, :description, :quantity, :category_id)
    end
end
