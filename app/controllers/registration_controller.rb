class RegistrationController < ApplicationController
  def register
    @user = User.new(user_params)

    if @user.save
      a = address_params
      custom_params = {
        user_id: @user.id,
        menu_name: "Endereço Principal",
        reciver: @user.name,
        cep:a["cep"],
        district:a["district"],
        city:a["city"],
        state:a["state"],
        street:a["street"],
        extra:a["extra"],
        number:a["number"]
      }
      @adress = Adress.new(custom_params)
      
      if @adress.save
        render json: @user
      else
        render json: @adress.errors, status: :unprocessable_entity
        User.find(@user.id).delete
      end

    else
      render json: @user.errors, status: :unprocessable_entity
    end 

  end
  
  private
    def user_params
      params.require(:user).permit(:name, :second_name, :email, :password, :password_confirmation, :cellphone_number, :phone_number)
    end
    def address_params
      params.require(:user).permit(:cep, :district, :city, :state, :street, :extra, :number)
    end
end
