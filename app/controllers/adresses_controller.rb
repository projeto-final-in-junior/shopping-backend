class AdressesController < ApplicationController
  before_action :set_adress, only: [:show, :update, :destroy]
  before_action :must_be_signed_in

  

  # GET /adresses
  def index
    if current_user.present?
      @adresses = Adress.where(user_id: current_user.id)

      render json: @adresses
    else
      render json: {message: "Must be logged in"}, status: 401
    end
  end

  # GET /adresses/1
  def show
    render json: @adress
  end

  # POST /adresses
  def create
    @adress = Adress.new(adress_params)

    if @adress.save
      render json: @adress, status: :created
    else
      render json: @adress.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /adresses/1
  def update
    if @adress.update(adress_params)
      render json: @adress
    else
      render json: @adress.errors, status: :unprocessable_entity
    end
  end

  # DELETE /adresses/1
  def destroy
    @adress.destroy
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adress
      @adress = Adress.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def adress_params
      params.require(:address).permit(:user_id, :menu_name, :cep, :district, :city, :state, :street, :extra, :number, :reciver)
    end
end
