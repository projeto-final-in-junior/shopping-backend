class PaymentMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.payment_mailer.confirm.subject
  #
  def confirm
    @user = params[:user]

    mail(to: @user.email, subject: "Confirmação de pagamento.") 
  end
end
