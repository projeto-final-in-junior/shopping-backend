class LikeComment < ApplicationRecord
  belongs_to :comment
  belongs_to :user
  validate :likecomment_already
  validate :own_comment


  def likecomment_already
    if LikeComment.find_by(user_id: user_id, comment_id: comment_id).present?
      errors.add(:likecomment_already_comment, "O usuário já deu like nesse comentário.")
    end
  end
  
  def own_comment
    if Comment.find(comment_id).user_id == user_id
      errors.add(:you_cant_like_your_own_comment, "Não se pode dar like no próprio comentário")
    end
  end
  
end
