class User < ApplicationRecord
    has_secure_password

    validates :email, :name ,:second_name ,presence: true
    validates :email, uniqueness: true
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i 
    validates :password_digest, presence: true
    validate :phone

    has_many :comments
    has_one :shopping_kart
    has_many :adress

    before_destroy :delete_related

    enum kind: {
        admin:0,
        user:1
    }

    def phone
        unless phone_number.nil?
            if phone_number != phone_number.to_i.to_s
                errors.add(:phone_numbers_must_be_numbers_only, "Números de telefone só pedem conter número")
            end
        end
        unless cellphone_number.nil?
            if cellphone_number != cellphone_number.to_i.to_s   
                errors.add(:phone_numbers_must_be_numbers_only, "Números de telefone só pedem conter número")
            end
        end
    end

    def delete_related
        Like.where(user_id: self.id).destroy_all
        Comment.where(user_id: self.id).destroy_all
        Adress.where(user_id: self.id).destroy_all
        ShoppingKart.where(user_id: self.id).destroy_all
    end
    
end
