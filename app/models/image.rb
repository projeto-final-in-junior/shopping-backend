class Image < ApplicationRecord
  belongs_to :product
  before_create :picture, presence: true
  mount_uploader :picture, PictureUploader
end
