class Like < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validate :already_liked
  after_save :update_like_ammount
  after_destroy :update_like_ammount

  def already_liked
    if Like.find_by(user_id: user_id, product_id: product_id).present?
      errors.add(:already_liked_product, "O usuário já curtiu o produto")
    end
  end

  def update_like_ammount ##numa linha só, essa é pra vc Tavares
    Product.find(product_id).update_attributes(like_amount: Like.where(product_id: product_id).length)
  end
  
end
