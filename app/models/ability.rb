# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      can :manage , Comment, :user_id => user.id
    end
    if user.admin?
      can :manage, :all
    end
  end
end
