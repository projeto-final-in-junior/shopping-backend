class Text < ApplicationRecord
    validates :section, :content, presence: true
    validates :section, uniqueness: true
end
