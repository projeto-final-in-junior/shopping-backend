class Product < ApplicationRecord
    has_many :likes 
    has_many :comments
    belongs_to :category
    has_many :images

    validates :name, :price, :description, presence: true
    validates :name, uniqueness: true
    validate :preço
    validate :quantity_above_0_default_0

    before_destroy :delete_related_things

    after_update :change_history_name
    before_save :save_old_name

    
    

    def preço
        unless price.nil?
            unless price > 0
                errors.add(:must_be_a_natural_number, "O produto tem que valer mais do que 0")
            end
        end
    end

    def delete_related_things
        Like.where(product_id: self.id).destroy_all
        Comment.where(product_id: self.id).destroy_all
        Image.where(product_id: self.id).destroy_all
        history = History.where(descontinued: false, product_name: self.name)
        for x in history
            x.descontinued = true
            x.save
        end
        ShoppingKart.where(product_id: self.id).destroy_all
    end
 
    def quantity_above_0_default_0
        if self.quantity == nil
            self.quantity = 0
        elsif self.quantity < 0
            errors.add(:must_be_a_natural_number, "Número no estoque não pode ser negativo")
        end
    end
    def update_quantity(itens_bought)
        self.quantity = self.quantity - itens_bought
        self.save
    end
    
    def save_old_name
        unless self.id.nil?
            @old_name = Product.find(self.id).name    
        end        
    end   
     
    def change_history_name
        to_change = History.where(descontinued: false, product_name: @old_name)
        for x in to_change
            x.product_name = self.name
            x.save
        end
    end
    
end
