class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validates :content , presence: true
  before_destroy :destroy_related_things

  def destroy_related_things
    for x in LikeComment.where(comment_id: self.id)
        x.delete
    end
  end
end
