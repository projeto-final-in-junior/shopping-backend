class Adress < ApplicationRecord
    belongs_to :user 
    validates  :menu_name, :cep, :district, :city, :state, :street, :extra, :number,  presence: true
    validate :user_has_menu_name
    validate :save_five

    def save_five
        unless Adress.where(user_id: user_id).length < 5
            errors.add(:save_five_comment, "Já tem 5 endereços")
            
        end
    end
    def user_has_menu_name 
        if Adress.find_by(user_id: user_id, menu_name: menu_name).present?
            errors.add(:user_has_menu_name_comment, "O usuário já tem um menu_name")
        end
    end
end
