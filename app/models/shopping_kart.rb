class ShoppingKart < ApplicationRecord
  belongs_to :user
  belongs_to :product
  validates :quantity_to_be_bought, length:{minimum:1}
  validates :quantity_to_be_bought, presence: true
  before_create :add_products
  
  after_create :is_there_enough
  after_create :zero_destroy
   
  def add_products
    product_research = ShoppingKart.find_by(product_id: product_id,user_id: user_id )
    if product_research.present?
      soma = product_research.quantity_to_be_bought.to_i + quantity_to_be_bought.to_i
      self.quantity_to_be_bought = soma 
      product_research.destroy
    end
  end

  def is_there_enough
    stock = Product.find(product_id).quantity
    if quantity_to_be_bought > stock
      self.quantity_to_be_bought = stock
      self.save
    end
  end

  def zero_destroy
    if quantity_to_be_bought < 1
      self.destroy
    end
  end
  
end


