class Category < ApplicationRecord
    has_many :has_category
    validates :name ,uniqueness: true
    
    before_save :save_old_name
    after_update :change_history_name

    def save_old_name
        unless self.id.nil?
            @old_name = Category.find(self.id).name     
        end       
    end    
    def change_history_name
        to_change = History.where(descontinued: false, category_name: @old_name)
        for x in to_change
            x.category_name = self.name
            x.save
        end
    end
end
