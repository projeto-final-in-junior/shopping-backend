class ProductSerializer < ActiveModel::Serializer
    attributes :id, :name, :price, :description, :quantity
    attributes :like_amount
    attribute :comments_num
    has_many :comments
    has_one :category
    has_many :images
    

    def comments_num
        Comment.where(product_id: object.id).length
    end
    

end

