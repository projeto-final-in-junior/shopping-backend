class TextSerializer < ActiveModel::Serializer
  attributes :id, :content, :section
end
