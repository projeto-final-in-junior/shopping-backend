class ShoppingKartSerializer < ActiveModel::Serializer
  attributes :id, :quantity_to_be_bought
  has_one :product
  attribute :stock

  def stock
    Product.find(object.product_id).quantity    
  end
  
end
