class HistorySerializer < ActiveModel::Serializer
  attributes :id, :product_name, :quantity, :descontinued, :price, :category_name
end
