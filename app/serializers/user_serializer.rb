class UserSerializer < ActiveModel::Serializer
    attributes :id, :name,:second_name ,:email, :phone_number, :cellphone_number, :kind
    has_many :adress
end