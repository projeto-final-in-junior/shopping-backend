class CommentSerializer < ActiveModel::Serializer
  attributes :id
  attribute :name
  attribute :email
  attribute :likes
  attributes :content
  has_one :product

  def name
    User.find(object.user_id).name
  end

  def email
    User.find(object.user_id).email
  end

  def likes
    LikeComment.where(comment_id: object.id).length
  end
  
  
end
